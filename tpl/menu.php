<ul class="nav nav-pills nav-stacked">
    <li>
        <a href="index.php">Main</a>
    </li>
    <?php if(!isset($_SESSION["auth"])) { ?>
    <li>
        <a href="login.php">Login</a>
    </li>
    <?php } else  {
        if (mb_substr($_SERVER['SCRIPT_NAME'], mb_strlen($_SERVER['SCRIPT_NAME']) - 10) !== "create.php") {?>
    <li>
        <a href="create.php">Create Post</a>
    </li>
        <?php }
        if (isset($_SESSION["auth"])) {?>
    <li>
        <a href="logout.php">Logout</a>
    </li>
    <?php } }?>
</ul>