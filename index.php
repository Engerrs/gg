<?php

$config = include_once("config.php");
$arrPosts = [];
$filename = "db/posts.json";
if (file_exists($filename)) {
    $f = fopen($filename, "r");
    for ($i = 0; ($i < 5 && !feof($f)); $i++) {
        $tmp = json_decode(fgets($f), true);
        if (!is_null($tmp)) {
            $arrPosts[] = $tmp;
        }
    }
}
include_once("tpl/index.php");
